# community_update_identification_flows

# Diagrams illustrating potential flows of for community-driven classification of updates to the scholarly literature using CC as a vetting process.

> These diagrams have been created in [mermaid](https://mermaid-js.github.io/mermaid/#/)- a textual description language for diagrams. If you want to edit these, you may want to try an [interactive mermaid editor](https://mermaid-js.github.io/mermaid/#/).


The two diagrams illustrate two broad approaches. 

The starting point of the first approach is publication records that have already been classified by publishers via Crossmark.

The starting point of the second approach is reporting by the general public.

Both approaches overlap in that both *vet* the intial classification attempt via CC and both attempt to use the CC verification results to improve the training of the automated classifier. 

The secondary goal of both approaches is to see if we can create a useful tool for automatically detecting research outputs that have undergone significant updates. If tractable, such a tool could be used for pre-screening, detecting misclassifications, etc. 


## Based on DOI records containing CrossMarks

Crossref already has records that already have publisher-supplied update information in the form of Crossmarks. This process would help us to determine how good publishers are at labeling their updates. This process hass two stages of vetting:

1. An attempt at automated screening
2. C Crowd vetting - the results of which- are used to try and improve the process of automated screening.

```mermaid
graph TD
    SAMPLES_FROM_CROSSREF[Samples from Crossref] --> AUTO_CLASSIFIER
    AUTO_CLASSIFIER{Auto Classifier}
    AUTO_CLASSIFIER -->|YES| C_CROWD{C Crowd}
    AUTO_CLASSIFIER -->|NO| C_CROWD  
    C_CROWD -->|YES| CLASSIFIER_TRAINER
    C_CROWD -->|NO| CLASSIFIER_TRAINER
    CLASSIFIER_TRAINER -->|Update| AUTO_CLASSIFIER
    C_CROWD -->|YES| ADD_EVENT[Add event to Crossref]
```

## Based on reports of correction/retractions from the general public 

Ultimately we would like to enourage the general public to be able to report siginificant updates. Obviously, with infromation as sesnitive as retractions, we would want to be very sure that the reports were accurate and so we would want to vet them first. This process has three stages of vetting:

1. A captcha stage to prevent malicious bulk reporting.
2. An attempt at automated screening
3. C Crowd vetting - the results of which- are used to try and improve the process of automated screening.

```mermaid
graph TD
    Person --> REPORT[Report Retraction/Correction FORM]
    REPORT --> CAPTCHA{Captcha}
    CAPTCHA -->|YES| AUTO_CLASSIFIER{Auto Classifier}
    CAPTCHA -->|NO| TRASH[Trash]
    AUTO_CLASSIFIER -->|YES| C_CROWD{C Crowd}
    AUTO_CLASSIFIER -->|NO| NOTIFY_NO[Notify Person No]
    NOTIFY_NO --> TRASH
    C_CROWD -->|YES| NOTIFY_YES[Notify Person Yes]
    C_CROWD -->|NO| NOTIFY_NO
    NOTIFY_NO --> CLASSIFIER_TRAINER[Classifier Trainer]
    CLASSIFIER_TRAINER -->|Update| AUTO_CLASSIFIER
    NOTIFY_YES --> CLASSIFIER_TRAINER[Classifier Trainer]
    NOTIFY_YES --> ADD_EVENT[Add report to Crossref]
```

  
